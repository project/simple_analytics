<?php

namespace Drupal\simple_analytics\Tests;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Database;

/**
 * Simple Analytics Data processing tests.
 *
 * @group Simple Analytics
 */
class SimpleAnalyticsDataTestBase extends SimpleAnalyticsTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Insert data.
    $file = drupal_get_path('module', 'simple_analytics') . "/src/Tests/sadata.json";
    $datas = Json::decode(file_get_contents($file));
    $con = Database::getConnection();

    foreach ($datas as $key => $data) {
      $data = SATData::prepareDates($data);
      foreach ($data as $row) {
        $query = $con->insert("simple_analytics_$key");
        $query->fields($row);
        $query->execute();
      }
    }

    // Init data count.
    $dbs = [
      // 8 lines from .json and 7 for previous days.
      'archive' => (8 + 7),
      'data' => 474,
      'visit' => 79,
    ];
    foreach ($dbs as $key => $nb) {
      $count = $con->select("simple_analytics_$key", 't')
        ->countQuery()
        ->execute()
        ->fetchField();
      $this->assertEqual($count, $nb, "Data count ($count) on '$key' is OK");
    }

  }

  /**
   * Test.
   */
  public function testInitCounts() {

  }

}
