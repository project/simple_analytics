<?php

namespace Drupal\simple_analytics\Tests;

use Drupal\Component\Serialization\Json;

/**
 * Simple Analytics Helper functions for tests.
 */
class SATHelper {

  /**
   * Prepare data and export.
   */
  public static function prepareDataArray() {

    // Data files dir.
    $dir = "/home/me/Desktop/www/packages/DATA/simple_analytics";

    // Reference date (Data backuped date).
    $date_ref = strtotime("2018-06-24");
    // Timestamp fields to process.
    $tsfields = ['date', 'timestamp', 'timeup'];
    $files = ['archive', 'data', 'visit'];
    $dataout = [];
    foreach ($files as $file) {
      $filepath = $dir . "/simple_analytics_$file.json";
      $data = Json::decode(file_get_contents($filepath));
      foreach ($data as $i => $row) {
        foreach ($tsfields as $tsfield) {
          if (isset($row[$tsfield])) {
            $data[$i][$tsfield] = ($row[$tsfield] - $date_ref);
          }
        }
      }
      $dataout[$file] = $data;
    }

    $string = Json::encode($dataout);
    file_put_contents('sadata.json', $string);
  }

}
