<?php

namespace Drupal\simple_analytics\Tests;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Database;

/**
 * Simple Analytics Test data processing functions for tests.
 */
class SATData {

  /**
   * Init simple analytics data.
   */
  public static function initData() {

    $file = drupal_get_path('module', 'simple_analytics') . "/src/Tests/sadata.json";
    $datas = Json::decode(file_get_contents($file));

    $con = Database::getConnection();
    foreach ($datas as $key => $data) {
      $data = SATData::prepareDates($data);
      foreach ($data as $row) {
        $query = $con->insert("simple_analytics_$key");
        $query->fields($row);
        $query->execute();
      }
    }
  }

  /**
   * Drop (truncate) current data.
   */
  public static function truncateData() {
    $dbs = ['archive', 'data', 'visit'];
    $con = Database::getConnection();
    foreach ($dbs as $key) {
      $con->truncate("simple_analytics_$key")->execute();
    }
  }

  /**
   * Fix dates.
   */
  public static function prepareDates($data, $shift = 0) {

    $date_ref = strtotime(date("Y-m-d")) + ($shift * 86400);
    $tsfields = ['date', 'timestamp', 'timeup'];
    $ipfields = ['REMOTE_ADDR'];
    foreach ($data as $i => $row) {
      foreach ($tsfields as $field) {
        if (isset($row[$field])) {
          $data[$i][$field] = ($row[$field] + $date_ref);
        }
      }
      foreach ($ipfields as $field) {
        if (isset($row[$field])) {
          $t = explode(".", $row[$field]);
          $t[3] = '0';
          $data[$i][$field] = implode(".", $t);
        }
      }
    }
    return $data;
  }

}
