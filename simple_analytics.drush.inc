<?php

/**
 * @file
 * Simple Analytics module (simple_analytics).
 *
 * This file provide drush commands tasks.
 */

use Drupal\Core\Database\Database;
use Drupal\simple_analytics\SimpleAnalyticsActions;

/**
 * Implements hook_drush_command().
 */
function simple_analytics_drush_command() {
  $items = [];

  // Check an entity type.
  $items['simple-analytics'] = [
    'description' => 'Simple analytics actions',
    'aliases' => ['san'],
    'arguments' => [
      'action' => dt('The action'),
    ],
    'options' => [
      'show' => dt('Show the db counts'),
    ],
    'examples' => [
      'drush san --show' => 'Show the number of records.',
      'drush san archive' => 'Execute data archive process.',
      'drush san flush' => 'Delete previuos data.',
      'drush san flushall' => 'Delete all data on DATA and VISIT tables.',
    ],
  ];

  // Drush command for module tests.
  $items['simple-analytics-tests'] = [
    'description' => 'Simple analytics actions',
    'aliases' => ['san-test'],
  ];

  return $items;
}

/**
 * Call back function of simple-analytics.
 */
function drush_simple_analytics($action = NULL) {

  $con = Database::getConnection();

  // Show the number of records.
  if (drush_get_option('show')) {
    $res = $con->select('simple_analytics_data', 't')
      ->countQuery()
      ->execute()
      ->fetchField();
    drush_log("Number of records on DATA table : $res", 'ok');
    $res = $con->select('simple_analytics_visit', 't')
      ->countQuery()
      ->execute()
      ->fetchField();
    drush_log("Number of records on VISIT table : $res", 'ok');
    $res = $con->select('simple_analytics_archive', 't')
      ->countQuery()
      ->execute()
      ->fetchField();
    drush_log("Number of records on ARCHIVE table : $res", 'ok');
    return;
  }

  if ($action == 'archive') {
    SimpleAnalyticsActions::archive();
  }
  elseif ($action == 'flush') {
    SimpleAnalyticsActions::flushPreviousData();
  }
  elseif ($action == 'flushall') {
    if (drush_confirm('Want you delete all records on DATA and VISIT tables ?')) {
      $query = $con->delete('simple_analytics_data');
      $query->execute();
      $query = $con->delete('simple_analytics_visit');
      $query->execute();
    }
  }
}

/**
 * Call back function of simple-analytics-tests.
 */
function drush_simple_analytics_tests() {
  // @todo : Execute as needed.
  drush_log("SATesting process end", 'ok');
}
